#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string>
#include <thread>
#include <atomic>

const int EXIT_APP = 0;
const int REGISTER = 1;
const int LOGIN = 2;
const int EXIT_LOGIN = 3;
const int START_CHAT = 4;

using namespace std;

string CheckUsernameString(const string& username) {
  string result = string("3 ");
  result += username;
  return result;
}

string RegisterString(const string& username, const string& password) {
  string result = string("4 ");
  result += username; result += string(" ");
  result += password;
  return result;
}

string StartChatString(const string& username) {
  string result = string("0 ");
  result += username;
  return result;
}

string SendMessageString(const string& username, const string& message) {
  string result = string("1 ");
  result += username; result += " ";
  result += message;
  return result;
}

string LogInString(const string& username, const string& password) {
  string result = string("5 ");
  result += username; result += " ";
  result += password;
  return result;
}

string ExitString() {
  return string("6");
}

string MessageReceivedString() {
  return string("2 1");
}

int BuildConnection(const string& serverIP, const int& serverPort) {
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  fcntl(sock, F_SETFL, O_NONBLOCK);

  struct sockaddr_in serv_addr;
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(serverIP.c_str());
  serv_addr.sin_port = htons(serverPort);
  connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

  return sock;
}

int SendString(const int& sock, const string& str) {
  printf("SendString(%s)\n", str.c_str());
  return write(sock, str.c_str(), str.length());
}

string ReceiveString(const int& sock) {
  char buffer[200];
  int sz = read(sock, buffer, sizeof(buffer) - 1);
  if(sz == -1) return string("");
  const string message = string(buffer, sz);
  
  printf("Message from server:%s\n", message.c_str());
  return message;
}

int SendMessage(const int& sock, const string& username, const string& message) {
  SendString(sock, SendMessageString(username, message));
  return 0;
}

int StartChat(const int& sock, const string& username) {
  SendString(sock, StartChatString(username));

  string result = string("");
  for(;result == string("");) {
    result = ReceiveString(sock);
  }

  if(result[0] == '0') {
    if(result.length() == 1) {
      return 2; // uSER_NOT_EXIST
    } else {
      return result[2] == '1';
    }
  } else {
    printf("Something goes wrong... Please try again.\n");
    return -1;
  }
}

int HasUsernameBeenUsed(const int& sock, const string& username) {
  printf("Checking if %s has been used...\n", username.c_str());
  
  SendString(sock, CheckUsernameString(username));
  
  string result = string("");
  for(;result == string("");) {
    result = ReceiveString(sock);
  }
  
  if(result[0] == '3') {
    return result[2] == '1';
  } else {
    printf("Something goes wrong... Please try again.\n");
    return -1;
  }
}

int Register(const int& sock) {
  string username, password;
  
  printf("Please enter your username: ");
  cin >> username;
  while(HasUsernameBeenUsed(sock, username) != 1) {
    printf("The username has been used by others. Please try another one: ");
    cin >> username;
  }

  printf("Username %s is valid to be used.\n", username.c_str());
  printf("Please enter your password: ");
  cin >> password;

  SendString(sock, RegisterString(username, password));

  string result = string("");
  for(;result == string("");) {
    result = ReceiveString(sock);
  }

  if(result[0] == '4') {
    return result[2] == '1';
  } else {
    printf("Something goes wrong... Please try again.\n");
    return -1;
  }
}

atomic<bool> IsChatting;
int KeepReceivingMessage(const int& sock) {
  while(IsChatting) {
    string str = ReceiveString(sock);
    if(str == "") continue;
    if(str[0] != '2') {
      printf("Something is wrong... Please try again later.\n");
      return -1;
    } else {
      string username, timestamp, message;
      int i = 2;
      for(; i<(int)str.length(); i++) {
        if(str[i] == ' ') break;
        username += str[i];
      }
      for(i++; i<(int)str.length(); i++) {
        if(str[i] == ' ') break;
        timestamp += str[i];
      }
      for(i++; i<(int)str.length(); i++) {
        message += str[i];
      }

      printf("[%s][%s]:%s\n", username.c_str(), timestamp.c_str(), message.c_str());
      SendString(sock, MessageReceivedString());
    }
  }

  return 0;
}

int Chat(const int& sock, const string& username, const bool& isUserOnline) {
  if(isUserOnline == 1) {
    printf("%s is online! Chat with him/her now!\n", username.c_str());
  } else {
    printf("%s is offline now. Leave him/her a message.\n", username.c_str());
  }

  IsChatting = true;
  thread mThread (KeepReceivingMessage, sock);

  printf("Type \"!File\" to send file. Type \"!Leave\" to leave\n");

  char _message[1024];
  string message;
  fgets(_message, 1024, stdin);
  for(;;) {
    printf("Write your message here. Max size is 1024 characters: ");

    fgets(_message, 1024, stdin);
    message = string(_message);
    message = string(message, 0, message.length() - 1);
    // if(message.length() == 1) continue;
    printf("%d:%s\n", (int)message.length(), message.c_str());

    int isLeaving = message.compare(string("!Leave"));
    int isFile = message.compare(string("!File"));
    printf("isLeaving:%d isFile:%d\n", isLeaving, isFile);
    if(isLeaving == 0) {
      break;
    } else if(isFile == 0) {
      printf("Enter the file path: ");
      string path;
      cin >> path;
      // TODO: Send file through socket
    } else {
      SendMessage(sock, username, message);
    }
  }

  IsChatting = false;
  mThread.join();

  return 0;
}

int LogIn(const int& sock) {
  string username, password;
  printf("username: ");
  cin >> username;
  printf("password: ");
  cin >> password;

  SendString(sock, LogInString(username, password));

  string result = string("");
  for(;result == string("");) {
	result = ReceiveString(sock);
  }

  if(result[0] == '5'){
	if(result[2] == '1'){
		if(result[4] == '1'){
			return 1;
		}
		else{
        		printf("Password incorrect.\n");
 			return 2;
		}
	}
	else{
		printf("%s doesn't exist. Try again.\n", username.c_str());
		return 3;
	}
  }
  else{
	printf("Something goes wrong... Please try again.\n");
	return -1;
  }
}

int GetUserIntent(const bool& isLoggedIn) {
  int intent;
  for(;;){ 
	if(isLoggedIn) {
 		printf("Please input a number. 0 to exit, 1 for starting a chat: ");
		cin >> intent;
		if(intent == 0 || intent == 1) {
			intent += 3;
			break;
		}
	}
	else{
		printf("Please input a number. 0 to exit, 1 for registration, 2 for login: ");
		cin >> intent;
		if(intent == 0 || intent == 1 || intent == 2) {
			break;
		}
	}
  }
  return intent;
}

int ExitApp(const int& sock) {
  SendString(sock, ExitString());
  close(sock);
  return 1;
}

const string serverIP = "127.0.0.1";
const int serverPort = 1234;

int main(){
  int intent, sock=BuildConnection(serverIP, serverPort), isLoggedIn=0;
  // printf("%d\n", sock);
  // if(sock < 0) {
  //   printf("Server offline. Please try again later.");
  //   return 0;
  // }
  for(;;){
	intent = GetUserIntent(isLoggedIn);
	if(intent == EXIT_APP){
		ExitApp(sock);
		printf("Bye.\n");
		break;
	}
	else if(intent == REGISTER){
		int result = Register(sock);
		if(result == 1){
			printf("Registration succeeds!\n");
		}
		else{
			printf("Something goes wrong... Please try again later.\n");
		}
	}
	else if(intent == LOGIN){
		int logInResult = LogIn(sock);
		if(logInResult == 1){
			isLoggedIn = 1;
		}
	}
	else if(intent == EXIT_LOGIN){
		isLoggedIn = 0;
	}
	else if(intent == START_CHAT) {
		string username;
		for(;;){
			printf("Input the user you want to chat with. Enter 0 to exit: ");
			cin >> username;
			if(username.length() == 1 && username[0] == '0')
				break;
			int result = StartChat(sock, username);
			if(result == 2){
				printf("Invalid username. Please try again.\n");
			}
			else{
				Chat(sock, username, result);
				break;
			}
		}
	}
  }
  close(sock);
  return 0;
}