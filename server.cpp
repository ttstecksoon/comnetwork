#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <thread>

using namespace std;

const string serverIP = "127.0.0.1";
const int serverPort = 1234;

int SendString(const int& sock, const string& str) {
  printf("SendString(%s)\n", str.c_str());
  return write(sock, str.c_str(), str.length());
}

string ReceiveString(const int& sock) {
  char buffer[500];
  int sz = read(sock, buffer, sizeof(buffer) - 1);
  const string message = string(buffer, sz);
  printf("Message from client: %s\n", message.c_str());
  return message;
}

int CheckUsername(const int& sock, const string& username) {
  FILE *fp;
  fp=fopen("userdata.txt", "a+");
  assert(fp!=NULL);
  char Data[1024];
  while(fgets(Data, 1024, fp)!=NULL){
	string User (Data, 0, strlen(Data)-2);
	if(username.compare(User)==0){
		SendString(sock, "3 0");
		fclose(fp);
		return 1;
	}
  }
  fclose(fp);
  SendString(sock, "3 1");
  return 0;
}

int Register(const int& sock, const string& username, const string& password) {
  FILE *fp;
  fp=fopen("userdata.txt", "a");
  assert(fp!=NULL);
  const char *userN=username.c_str();
  const char *passw=password.c_str();
  fputs(userN, fp);
  fputs("\n", fp);
  fputs(passw, fp);
  fputs("\n", fp);
  fclose(fp);
  SendString(sock, "4 1");
  return 0;
}

int LogIn(const int& sock, const string& username, const string& password) {
  FILE *fp;
  fp=fopen("userdata.txt", "a+");
  assert(fp!=NULL);
  char Data[1024];
//const char *userN=username.c_str();
  while(fgets(Data, 1024, fp)!=NULL){
	string User (Data, 0, strlen(Data)-2);
	if(username.compare(User)==0){
		fgets(Data, 1024, fp);
		string Passw (Data, 0, strlen(Data)-1);
		if(password.compare(Passw)==0){
			SendString(sock, "5 1 1");
		}
		else{
			SendString(sock, "5 1 0");
		}
		fclose(fp);
		return 1;
	}
  }
  fclose(fp);
  SendString(sock, "5 0");
  return 0;
}

int StartChat(const int& sock, const string& username) {
  FILE *fp;
  fp=fopen("userdata.txt", "a+");
  assert(fp!=NULL);
  char Data[1024];
  while(fgets(Data, 1024, fp)!=NULL){
	string User (Data, 0, strlen(Data)-2);
	if(username.compare(User)==0){
		SendString(sock, "0 0");
		fclose(fp);
		return 1;
	}
  }
  fclose(fp);
  SendString(sock, "0");
  return 0;
}

int SendMessage(const int& sock, const string& username, 
		const string& timestamp, const string& message){
	SendString(sock, "2 user 2018-12-31-23:59:59 Hello client!");
	const string res = ReceiveString(sock);
	return 0;
}

int ReceiveMessage(const int& sock) {
  SendString(sock, "1 1");
  return 0;
}

int Exit(const int& sock) {
  close(sock);
  return 1;
}

int serv_sock;

int BuildConnection() {
  serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  
  struct sockaddr_in serv_addr;
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(serverIP.c_str());
  serv_addr.sin_port = htons(serverPort);
  bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

  listen(serv_sock, 20);

  struct sockaddr_in clnt_addr;
  socklen_t clnt_addr_size = sizeof(clnt_addr);
  int clnt_sock = accept(serv_sock, (struct sockaddr*)& clnt_addr, & clnt_addr_size);

  return clnt_sock;
}

int main() {
  int sock = BuildConnection();
  for(;;){
	string str = ReceiveString(sock);
	if(str[0] == '0'){ // STARTCHAT
		string userN (str, 2, str.length()-1);
		StartChat(sock, userN);
		/*for(int i=0; i<1; i++){
        		SendMessage(sock, "user", "2019-01-01-23:59:59", "Hello client!");
		}*/
	}
	else if(str[0] == '1'){ // SendMessage(Client)
		//ReceiveMessage(sock);
	}
	else if(str[0] == '2'){ // SendMessage(Server)
      
 	}
	else if(str[0] == '3'){ // CheckUsername
		string userN (str, 2, str.length()-1);
		CheckUsername(sock, userN);
	}
	else if(str[0] == '4'){ // Register
		int index=2;
		for(int i=2; i<str.length(); i++){
			if(str[i]==' ')
				index=i;
		}
		string userN (str, 2, index-1);
		string passw (str, index+1, str.length()-1);
		Register(sock, userN, passw);
	}
	else if(str[0] == '5'){ // LogIn
		int index=2;
		for(int i=2; i<str.length(); i++){
			if(str[i]==' ')
				index=i;
		}
		string userN (str, 2, index-2);
		string passw (str, index+1, str.length()-1);
		LogIn(sock, userN, passw);
	}
	else if(str[0] == '6'){ // Exit
		Exit(sock);
		break;
	}
  }
  close(serv_sock);
}