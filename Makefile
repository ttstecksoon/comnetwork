all: server client

server:
	g++ -std=c++11 server.cpp -o server -Wall -Wextra -O2

client:
	g++ -std=c++11 -pthread client.cpp -o client -Wall -Wextra -O2

clean:
	rm server client
